
const FIRST_NAME = "Lorena";
const LAST_NAME = "Spinu";
const GRUPA = "1083";


function initCaching() {
    
    var obiect = new Object();
    obiect.pageAccessCounter = function (param) {
	    var param_1 = String(param).toLowerCase();

        if (param == null && obiect.hasOwnProperty('home')==true)
            obiect['home'] = obiect['home'] + 1; 
        else if (param == null && obiect.hasOwnProperty('home') == false)
            obiect['home'] = 1; 
        else {
            
            if (obiect.hasOwnProperty(param_1)==true)
                obiect[param_1] = obiect[param_1] + 1;
            else
                obiect[param_1] = 1;
        }
    };
    obiect.getCache = function () {
        return obiect;
    };
    return obiect;
}


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

